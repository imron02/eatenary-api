var user    = require('../models/User');
var _       = require('lodash');

module.exports = {
    /**
     * Display a listing of the user.
     *
     */
    index: function (req, res){
        var filters     = req.query;
        var collection  = user.find();

        if(!_.isEmpty(filters)) {
            _.forEach(filters, function(value, key) {
                collection.where(key).equals(value);
            });
        }

        collection.exec(function (err, users) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, null, null, users);
        });
    },

    /**
     * Display the specified user.
     *
     */
    show: function (req, res) {
        var id = req.params.id;

        user.findById(id, function (err, user) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, null, null, user);
        });
    },

    /**
     * Store a newly created user in storage.
     *
     */
    store: function (req, res) {
        var data = req.body;

        user.create(data, function (err) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, "Data user already save"); 
        });
    },

    /**
     * Update the specified user in storage.
     *
     */
    update: function (req, res) {
        var id   = req.params.id;
        var data = req.body;

        user.findById(id, function (err, user) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            user = _.assignIn(user, data)
            user.save(function (err) {
                if (err) {
                    return res.apiResponse(400, null, err);
                }

                res.apiResponse(200, "Data user already update"); 
            });

        });
    },

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    destroy: function (req, res) {
        var id = req.params.id;

        user.remove({"_id": id}, function (err) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, "User already removed"); 
        })
    }
};