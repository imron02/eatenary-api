var country = require('../models/Country');
var _ = require('lodash');

module.exports = {
    /**
     * Display a listing of the country.
     *
     */
    index: function (req, res){
        var filters     = req.query;
        var collection  = country.find();

        if(!_.isEmpty(filters)) {
            _.forEach(filters, function(value, key) {
                collection.where(key).equals(value);
            });
        }

        collection.exec(function (err, countries) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, null, null, countries);
        });
    },

    /**
     * Display the specified country.
     *
     */
    show: function (req, res) {
        var id = req.params.id;

        country.findById(id, function (err, country) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, null, null, country);
        });
    },

    /**
     * Store a newly created country in storage.
     *
     */
    store: function (req, res) {
        var data = req.body;

        country.create(data, function (err) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, "Data country already save"); 
        });
    },

    /**
     * Update the specified country in storage.
     *
     */
    update: function (req, res) {
        var id   = req.params.id;
        var data = req.body;

        country.findById(id, function (err, country) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            country = _.assignIn(country, data)
            country.save(function (err) {
                if (err) {
                    return res.apiResponse(400, null, err);
                }

                res.apiResponse(200, "Data country already update"); 
            });

        });
    },

    /**
     * Remove the specified country from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    destroy: function (req, res) {
        var id = req.params.id;

        country.remove({"_id": id}, function (err) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, "User already removed"); 
        })
    }
};