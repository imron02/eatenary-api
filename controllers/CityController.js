var city = require('../models/City');
var _    = require('lodash');

module.exports = {
    /**
     * Display a listing of the city.
     *
     */
    index: function (req, res){
        var filters     = req.query;
        var collection  = city.find();

        if(!_.isEmpty(filters)) {
            _.forEach(filters, function(value, key) {
                collection.where(key).equals(value);
            });
        }

        collection.exec(function (err, cities) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, null, null, cities);
        });
    },

    /**
     * Display the specified city.
     *
     */
    show: function (req, res) {
        var id = req.params.id;

        city.findById(id, function (err, city) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, null, null, city);
        });
    },

    /**
     * Store a newly created city in storage.
     *
     */
    store: function (req, res) {
        var data = req.body;

        city.create(data, function (err) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, "Data city already save"); 
        });
    },

    /**
     * Update the specified city in storage.
     *
     */
    update: function (req, res) {
        var id   = req.params.id;
        var data = req.body;

        city.findById(id, function (err, city) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            city = _.assignIn(city, data)
            city.save(function (err) {
                if (err) {
                    return res.apiResponse(400, null, err);
                }

                res.apiResponse(200, "Data city already update"); 
            });

        });
    },

    /**
     * Remove the specified city from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    destroy: function (req, res) {
        var id = req.params.id;

        city.remove({"_id": id}, function (err) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, "User already removed"); 
        })
    }
};