var province = require('../models/Province');
var _ = require('lodash');

module.exports = {
    /**
     * Display a listing of the province.
     *
     */
    index: function (req, res){
        var filters     = req.query;
        var collection  = province.find();

        if(!_.isEmpty(filters)) {
            _.forEach(filters, function(value, key) {
                collection.where(key).equals(value);
            });
        }

        collection.exec(function (err, provinces) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, null, null, provinces);
        });
    },

    /**
     * Display the specified province.
     *
     */
    show: function (req, res) {
        var id = req.params.id;

        province.findById(id, function (err, province) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, null, null, province);
        });
    },

    /**
     * Store a newly created province in storage.
     *
     */
    store: function (req, res) {
        var data = req.body;

        province.create(data, function (err) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, "Data province already save"); 
        });
    },

    /**
     * Update the specified province in storage.
     *
     */
    update: function (req, res) {
        var id   = req.params.id;
        var data = req.body;

        province.findById(id, function (err, province) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            province = _.assignIn(province, data)
            province.save(function (err) {
                if (err) {
                    return res.apiResponse(400, null, err);
                }

                res.apiResponse(200, "Data province already update"); 
            });

        });
    },

    /**
     * Remove the specified province from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    destroy: function (req, res) {
        var id = req.params.id;

        province.remove({"_id": id}, function (err) {
            if (err) {
                return res.apiResponse(400, null, err);
            }

            res.apiResponse(200, "User already removed"); 
        })
    }
};