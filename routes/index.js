var api_response = require('./response.middleware');
var express      = require('express');
var router       = express.Router();
var rateLimit    = require('express-rate-limit');

// Controller
var user_controller     = require('../controllers/UserController');
var country_controller  = require('../controllers/CountryController');
var province_controller = require('../controllers/ProvinceController');
var city_controller     = require('../controllers/CityController');

module.exports = {
    /**
     * Basic rate-limiting middleware for Express. Use to limit repeated 
     * requests to public APIs and/or endpoints such as password reset. 
     * 
     */
    rateLimit: (function(){
        var api_limiter = new rateLimit({
            windowMs    : 15*60*1000, // 15 minutes
            // delayAfter  : 5, // begin slowing down responses after the first request
            // delayMs     : 3*1000, // disable delaying - full speed until the max limit is reached
            max         : 60,
            message     : "Too many request, please try again after an 15 minutes"
        });

        return api_limiter;
    })(),

    /**
     * Here is where you can register API routes for your application. These
     * routes are loaded by the express router.
     * 
     */
    routes: (function() {
        'use strict';
        
        // Initialize middlware
        router.use(api_response);

        // Index Route
        router.get('/', function(req, res, next) {
            res.render('index', { title: 'Express' });
        });

        // User route
        router.get('/user', user_controller.index);
        router.post('/user', user_controller.store);
        router.get('/user/:id', user_controller.show);
        router.put('/user/:id', user_controller.update);
        router.delete('/user/:id', user_controller.destroy);

        // Country route
        router.get('/country', country_controller.index);
        router.post('/country', country_controller.store);
        router.get('/country/:id', country_controller.show);
        router.put('/country/:id', country_controller.update);
        router.delete('/country/:id', country_controller.destroy);

        // Province route
        router.get('/province', province_controller.index);
        router.post('/province', province_controller.store);
        router.get('/province/:id', province_controller.show);
        router.put('/province/:id', province_controller.update);
        router.delete('/province/:id', province_controller.destroy);

        // City route
        router.get('/city', city_controller.index);
        router.post('/city', city_controller.store);
        router.get('/city/:id', city_controller.show);
        router.put('/city/:id', city_controller.update);
        router.delete('/city/:id', city_controller.destroy);

        return router;
    })(),
}
