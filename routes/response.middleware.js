module.exports = function(req, res, next) {
    res.apiResponse = function(statusCode, message = "", error = null, data = null) {
        // if response error
        if (statusCode != 200) {
            return res.status(400).json({
                'code'      : statusCode,
                'success'   : false,
                'message'   : message,
                'errors'    : error
            });
            
        }

        res.status(200).json({
            'code'      : statusCode,
            'success'   : true,
            'message'   : message,
            'errors'    : error,
            'data'      : data
        });
    };

    next();
};