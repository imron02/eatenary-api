var express      = require('express');
var path         = require('path');
var favicon      = require('serve-favicon');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var mongoose     = require('mongoose');
var router       = require('./routes');
var cors         = require('cors');

// Connect to mongodb
var db = mongoose.connection;
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/dev_eatenary');
db.on('error', function(err) {
    console.error(err.message);
});
db.once('open', function() {
    console.info('Database is connect');
});

// If node process end, close mongoose connection
var gracefulExit = function() {
    mongoose.connection.close(function () {
        console.log('Database connection is disconnected through app termination');
        process.exit(0);
    });
}
process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Enable cors
app.use(cors());

// Limiter request
app.use(router.rateLimit);

// Routing
app.use('/', router.routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
