var mongoose = require('mongoose');
var schema   = mongoose.Schema;

/**
 * Create schema
 *
 */
var countrySchema = new schema({
    name: {
        type    : String,
        required: true
    },
    created_at: Date,
    updated_at: Date
});

/**
 * Schema middleware
 *
 */
countrySchema.pre('save', function (next) {
    // Change update_at field to current date
    var currentDate = new Date();
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at) {
        this.created_at = currentDate;
    }

    next();
});

/**
 * Initialize schema and export schema
 *
 */
var country     = mongoose.model('country', countrySchema);
module.exports  = country;