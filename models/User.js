var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt   = require('bcrypt');
var moment   = require('moment');

// create a schema
var userSchema = new Schema({
    email: {
        type    : String, 
        required: true,
        unique  : true
    },
    password: {
        type    : String,
        required: true
    },
    first_name: {
        type    : String,
        required: true
    },
    last_name: {
        type    : String,
        required: true
    },
    gender: {
        type    : Number,
        enum    : [0, 1],
        required: true
    },
    birth_date  : String,
    handphone   : String,
    telephone   : String,
    country     : String,
    province    : String,
    city        : String,
    postalcode  : String,
    address     : String,
    status: {
        type    : Number,
        enum    : [0, 1],
        required: true
    },
    created_at  : Date,
    updated_at  : Date
});

// Shema middleware
userSchema.pre('save', function(next) {
    var user        = this;
    var currentDate = new Date();

    bcrypt.hash(this.password, 10, function(err, hash) {
        if (err) {
            return next(err);
        }

        // override the cleartext password with the hashed one
        user.password = hash;
        next();
    });

    // Convert birt date
    var birth_date      = moment(user.birth_date, 'DD/MM/YYYY');
    user.birth_date   = birth_date.format('YYYY-MM-DD');
  
    // change the updated_at field to current date
    user.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!user.created_at) {
        user.created_at = currentDate;
    }

    next();
});

// Initialize schema
var user = mongoose.model('user', userSchema);

// make this available to our users in our Node applications
module.exports = user;